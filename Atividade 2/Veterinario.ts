/*
    Criar uma classe Veterinario, com os seguintes atributos 
        nome e 
        numeroRegistro. 
    Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
    Além disso, a classe deverá ter o método construtor implementado. 
    Esta classe também deverá ter um método estático chamado obterValorConsulta, que irá receber um valor da enum TipoPet como parâmetro e retornar o valor da consulta de acordo com a seguinte regra 
        se for cachorro, o valor é 200 reais; 
        se for gato o valor é 150 reais; e 
        se for outro animal, o valor é 120 reais. 
    Por fim, a classe deverá conter um método chamado fazerConsulta, o qual irá receber um objeto da classe Pet como parâmetro e retornar uma string dizendo a seguinte mensagem Consultando o pet XYZ, substituindo XYZ pelo nome do pet.
*/
export class Veterinario {

    private nome: string;
    private numeroRegistro: number;
    
    public constructor(nome:string, numeroRegistro: number) {
        this.nome = nome;            
        this.numeroRegistro = numeroRegistro;
    }
         
    public getNome(): string {
        return this.nome;
    }
    
    public getNumeroRegistro(): number {
        return this.numeroRegistro;
    }
    
    public setNome(nome:string) {
        this.nome=nome;
    }
    
    public setNumeroRegistro(numeroRegistro: number) {
        this.numeroRegistro=numeroRegistro;
    }
    
    
    public static obterValorConsulta(){ //implementar

    }
    public fazerConsulta(){
        
    }
}