import { Cliente } from "./Cliente";
import { TipoPet } from "./TipoPet";
/*

    Criar uma classe Pet, com os seguintes atributos 
        nome, 
        tipo (respeitando os valores da enum), 
        dono (objeto da classe Cliente) e 
        peso. 
    Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
    Além disso, a classe deverá ter o método construtor implementado.

*/
export class Pet {

    private nome:string;
    private tipo:TipoPet;                                              
    private dono:Cliente;
    private peso:number;

    public constructor (nome:string, tipo:TipoPet, dono:Cliente, peso:number){
        this.nome=nome;
        this.tipo=tipo;
        this.dono=dono;
        this.peso=peso;
    }

    public getNome():string{        //getters
        return this.nome;
    }
    
    public getTipo():TipoPet{        //getters
        return this.tipo;
    }
    public getDono():Cliente{        //getters
        return this.dono;
    }
    
    public getpeso():number{
        return this.peso;
    }
    
    public setNome(nome:string){    //setters
        this.nome = nome;
    }
    public seTipo(tipo:TipoPet){    //setters
        this.tipo = tipo;
    }
    public setDono(dono:Cliente){    //setters
        this.dono = dono;
    }
    
    public setpeso (peso:number){
        this.peso = peso;
    }



}