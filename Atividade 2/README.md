# Atividade 2

Para este exercício, é recomendado utilizar a pasta modelo para projetos TS.
Dentro da pasta, deverão existir um arquivo para cada classe, bem como o arquivo de teste.

Criar uma enum TipoPet, contendo os seguintes valores CACHORRO, GATO, OUTRO.

Criar uma classe Cliente, com os seguintes atributos nome e cpf. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

Criar uma classe Pet, com os seguintes atributos nome, tipo (respeitando os valores da enum), dono (objeto da classe Cliente) e peso. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

Criar uma classe Veterinario, com os seguintes atributos nome e numeroRegistro. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Esta classe também deverá ter um método estático chamado obterValorConsulta, que irá receber um valor da enum TipoPet como parâmetro e retornar o valor da consulta de acordo com a seguinte regra se for cachorro, o valor é 200 reais; se for gato o valor é 150 reais; e se for outro animal, o valor é 120 reais. Por fim, a classe deverá conter um método chamado fazerConsulta, o qual irá receber um objeto da classe Pet como parâmetro e retornar uma string dizendo a seguinte mensagem Consultando o pet XYZ, substituindo XYZ pelo nome do pet.


Criar uma classe teste para trabalhar com as classes criadas acima.