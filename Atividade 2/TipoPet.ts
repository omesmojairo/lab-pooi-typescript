/*
        Criar uma enum TipoPet, contendo os seguintes valores CACHORRO, GATO, OUTRO.
*/

export enum TipoPet {
    CACHORRO,
    GATO,
    OUTRO
}