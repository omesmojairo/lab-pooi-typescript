/*
Criar uma classe Cliente, com os seguintes atributos 
    nome e 
    cpf. 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado.

*/
export class Cliente {

    private nome:string;
    private cpf:number;                                              

    public constructor (nome:string, cpf:number){
        this.nome=nome;
        this.cpf=cpf;
    }

    public getNome():string{        //getters
        return this.nome;
    }
    
    public getCpf():number{
        return this.cpf;
    }
    
    public setNome(nome:string){    //setters
        this.nome = nome;
    }
    
    public setCpf (cpf:number){
        this.cpf = cpf;
    }



}