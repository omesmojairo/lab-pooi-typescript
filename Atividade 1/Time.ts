
/*
7 - Criar uma classe abstrata Time, com os seguintes atributos 
    nome e 
    jogadores (vetor de objetos da classe Jogador). 
Para esta classe, todos os atributos deverão ser protegidos e, para acessá-los, os getters e setters também devem ser implementados, 
    exceto o setter para o atributo jogadores. 
Além disso, a classe deverá ter o método construtor implementado. 

Por fim, a classe precisa ter dois métodos adicionais 
    adicionarJogador, que será abstrato, recebendo um objeto da classe Jogador como parâmetro e retornando um valor booleano indicando se foi possível ou não adicionar o jogador; e 
    obterOverallTotal, que irá percorrer o vetor de jogadores e calcular a média do overall deles.

*/

import { Jogador } from "./Jogador";

export abstract class Time {                    //classe abstrata nao pode ser instanciada     

    protected nome:string;
    protected jogadores:Jogador[];     //importado de Jogador.ts
    
    
    public constructor (nome:string, jogadores:Jogador[]){
        this.nome=nome;
        this.jogadores=jogadores;
    }

    public getNome():string {                    //getters
        return this.nome;
    }
    public getJogadores():Jogador[] {                    //getters
        return this.jogadores;
    }
        
    public setNome(nome:string) {                  //setters
        this.nome=nome;
    }
            
    public abstract adicionarJogador(jogador:Jogador): boolean; //{              //classe abstrata que será implementada depois  
        //this.jogadores.push(jogador); //adiciona valor no final do vetor
        //falta implementar        
        
    //}

    public obterOverallTotal(){         
        let overallMedio:number=0;
        let overallTotal:number=0;
        for(let i = 0; i < this.jogadores.length; i++) {      //percorre o vetor do início até o final
            overallTotal += this.jogadores.overall[i];   
        }
        return overallMedio=overallTotal/this.jogadores.length; //calcula a media do overall os jogadores
        
    }
}
