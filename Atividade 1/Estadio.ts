import { Local } from "./Local";
import { TipoGrama } from "./TipoGrama";

/*4 - 
Criar uma classe Estadio, filha da classe Local, com o seguinte atributo 
    tipoGrama. 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado.
*/
export class Estadio extends Local {           

    private tipoGrama:TipoGrama;
        

    public constructor (tipoGrama:TipoGrama, nome:string, cidade:string, capacidade:number ){
        super(nome, cidade, capacidade);
        this.tipoGrama=tipoGrama;
      
    }

    public getTipoGrama():TipoGrama {                    //getters
        return this.tipoGrama;
    }
        
    public setTipoGrama(tipoGrama:TipoGrama) {                  //setters
        this.tipoGrama=tipoGrama;
    }
           
    
}
