
/*
10 - Criar uma classe TimeRugby, filha da classe Time, com o seguinte atributo 
        estadio (objeto da classe Estadio). 
    Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
    Além disso, a classe deverá ter o método construtor implementado. 
    
    Por fim, a classe deverá implementar o método abstrato a
        dicionarJogador respeitando a seguinte regra o vetor de jogadores nesta classe só pode ter até 15 jogadores. 
    
        Se a condição for satisfeita, o jogador deve ser adicionado ao vetor e o valor true deverá ser retornado, caso contrário, deverá ser retornado false.
*/
import { Estadio } from "./Estadio";
import { Jogador } from "./Jogador";
import { Time } from "./Time";

export class TimeRugby extends Time {                    

    private estadio:Estadio;
    
    public constructor (estadio:Estadio, nome:string, jogadores:Jogador[]){
        super(nome, jogadores);
        this.estadio=estadio;
    }

    public getEstadio() {                    //getters
        return this.estadio;
    }
            
    public setEstadio(estadio:Estadio) {                  //setters
        this.estadio=estadio;
    }
            
    public adicionarJogador(jogador:Jogador) {                
        let jogadores: Jogador[] = [];         //criar um vetor vazio
        this.jogadores.push(jogador);
        //if ()           //falta implementar       
    }

    
        
}
