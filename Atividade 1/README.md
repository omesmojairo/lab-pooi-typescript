Atividade 1

    Para este exercício, é recomendado utilizar a pasta modelo para projetos TS.
    Dentro da pasta, deverão existir um arquivo para cada classe, bem como o arquivo de teste.

Criar uma enum TipoGrama, contendo os seguintes valores: NATURAL, SINTETICA, MISTA.

Criar uma classe Jogador, com os seguintes atributos: nome e overall (representa a habilidade geral do jogador). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

Criar uma classe abstrata Local, com os seguintes atributos: nome, cidade e capacidade (quantidade de pessoas que cabem no local). Para esta classe, todos os atributos deverão ser protegidos e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

Criar uma classe Estadio, filha da classe Local, com o seguinte atributo: tipoGrama. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

Criar uma classe Ginasio, filha da classe Local, com o seguinte atributo: altura. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

Criar uma classe abstrata Time, com os seguintes atributos: nome e jogadores (vetor de objetos da classe Jogador). Para esta classe, todos os atributos deverão ser protegidos e, para acessá-los, os getters e setters também devem ser implementados, exceto o setter para o atributo jogadores. Além disso, a classe deverá ter o método construtor implementado. Por fim, a classe precisa ter dois métodos adicionais: adicionarJogador, que será abstrato, recebendo um objeto da classe Jogador como parâmetro e retornando um valor booleano indicando se foi possível ou não adicionar o jogador; e obterOverallTotal, que irá percorrer o vetor de jogadores e calcular a média do overall deles.

Criar uma classe TimeFutebol, filha da classe Time, com o seguinte atributo: estadio (objeto da classe Estadio). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, a classe deverá implementar o método abstrato adicionarJogador respeitando a seguinte regra: o vetor de jogadores nesta classe só pode ter até 11 jogadores. Se a condição for satisfeita, o jogador deve ser adicionado ao vetor e o valor true deverá ser retornado, caso contrário, deverá ser retornado false.

Criar uma classe TimeBasquete, filha da classe Time, com o seguinte atributo: ginasio (objeto da classe Ginasio). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, a classe deverá implementar o método abstrato adicionarJogador respeitando a seguinte regra: o vetor de jogadores nesta classe só pode ter até 5 jogadores. Se a condição for satisfeita, o jogador deve ser adicionado ao vetor e o valor true deverá ser retornado, caso contrário, deverá ser retornado false.

Criar uma classe TimeRugby, filha da classe Time, com o seguinte atributo: estadio (objeto da classe Estadio). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, a classe deverá implementar o método abstrato adicionarJogador respeitando a seguinte regra: o vetor de jogadores nesta classe só pode ter até 15 jogadores. Se a condição for satisfeita, o jogador deve ser adicionado ao vetor e o valor true deverá ser retornado, caso contrário, deverá ser retornado false.

Criar uma classe TimeVolei, filha da classe Time, com o seguinte atributo: ginasio (objeto da classe Ginasio). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, a classe deverá implementar o método abstrato adicionarJogador respeitando a seguinte regra: o vetor de jogadores nesta classe só pode ter até 6 jogadores. Se a condição for satisfeita, o jogador deve ser adicionado ao vetor e o valor true deverá ser retornado, caso contrário, deverá ser retornado false.

Criar uma classe teste para trabalhar com as classes criadas acima.
