import { Local } from "./Local";

/*
5- Criar uma classe Ginasio, filha da classe Local, com o seguinte atributo 
    altura. 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado.

*/
export class Ginasio extends Local {           

    private altura:number;
        

    public constructor (altura:number, nome:string, cidade:string, capacidade:number ){    //atributos de Local.ts
        super(nome, cidade, capacidade);
        this.altura=altura;
      
    }

    public getAltura():number {                    //getters
        return this.altura;
    }
        
    public setAltura(altura:number) {                  //setters
        this.altura=altura;
    }
           
    
}
