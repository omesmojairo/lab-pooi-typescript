
/*
10 - Criar uma classe TimeVolei, filha da classe Time, com o seguinte atributo 
    ginasio (objeto da classe Ginasio). 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado. 
Por fim, a classe deverá implementar o método abstrato adicionarJogador respeitando a seguinte regra o vetor de jogadores nesta classe só pode ter até 6 jogadores. 
Se a condição for satisfeita, o jogador deve ser adicionado ao vetor e o valor true deverá ser retornado, caso contrário, deverá ser retornado false.
*/
import { Ginasio } from "./Ginasio";
import { Jogador } from "./Jogador";
import { Time } from "./Time";

export class TimeVolei extends Time {                    

    private ginasio:Ginasio;
    
    public constructor (ginasio:Ginasio, nome:string, jogadores:Jogador[]){
        super(nome, jogadores);
        this.ginasio=ginasio;
    }

    public getGinasio() {                    //getters
        return this.ginasio;
    }
            
    public setGinasio(ginasio:Ginasio) {                  //setters
        this.ginasio=ginasio;
    }
            
    public adicionarJogador(jogador:Jogador) {                
        let jogadores: Jogador[] = [];         //criar um vetor vazio
        this.jogadores.push(jogador);
        //if ()           //falta implementar       
    }

    
        
}
