/*
2-  Criar uma classe Jogador, com os seguintes atributos 
        nome e 
        overall (representa a habilidade geral do jogador). 
    Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
    Além disso, a classe deverá ter o método construtor implementado.

*/
export class Jogador {

    private nome:string;
    private overall:number;                                              

    public constructor (nome:string, overall:number){
        this.nome=nome;
        this.overall=overall;
    }

    public getNome():string{        //getters
        return this.nome;
    }
    
    public getOverall():number{
        return this.overall;
    }
    
    public setNome(nome:string){    //setters
        this.nome = nome;
    }
    
    public setOverall (overall:number){
        this.overall = overall;
    }



}