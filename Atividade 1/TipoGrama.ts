/*
 1-   Criar uma enum TipoGrama, contendo os seguintes valores 
        NATURAL, 
        SINTETICA, 
        MISTA.

*/

export enum TipoGrama {
    NATURAL,
    SINTETICA,
    MISTA
}