/*
3 - Criar uma classe abstrata Local, com os seguintes atributos 
    nome, 
    cidade e 
    capacidade (quantidade de pessoas que cabem no local). 
Para esta classe, todos os atributos deverão ser protegidos e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado.

*/

export abstract class Local {                    //classe abstrata nao pode ser instanciada     

    protected nome: string;
    protected cidade: string;
    protected capacidade: number;
    
    
    public constructor (nome:string, cidade:string, capacidade:number){
        this.nome=nome;
        this.cidade=cidade;
        this.capacidade=capacidade;
    }

    public getNome():string {                    //getters
        return this.nome;
    }
        
    public getCidade():string {                    //getters
        return this.cidade;
    }
    public getCapacidade():number {                    //getters
        return this.capacidade;
    }
    
    public setNome(nome:string) {                  //setters
        this.nome=nome;
    }
    public setCidade(cidade:string) {                  //setters
        this.cidade=cidade;
    }
    
    public setCapacidade(capacidade:number) {                  //setters
        this.capacidade=capacidade;
    }
    
        
    }
